<?php
/**Created by B. Ros**/
class Template{
	public $vars = array();
	public $html = "";
	
	public function __construct($templateFile){
		$templateFile = "templates/".$templateFile;
		if(file_exists($templateFile))
			$this->html = file_get_contents($templateFile);
		else
			throw new ErrorExeption('Kan geen template vinden haha!');
	}
	public function templateVar($name,$value){
		$this->vars[$name] = $value;
	}
	public function setTemplateVars($templateVars){
		$this->vars = $templateVars;
	}
	public function render(){
		foreach ($this->vars as $key => $value){
			$this->html = str_replace('#{'.$key.'}',$value,$this->html);
		}
	}
	public function parse($returnOnly=false){
		if(!$returnOnly){
			echo $this->html;
		}
		return $this->html;
	}
}
?>